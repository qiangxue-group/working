DECLARE
V_COUNT          INTEGER;
  v_policy_code    prod_db.t_contract_master.policy_code%type := '%inputParameter_0%';
  v_collect_pay    prod_db.t_bank_text_detail.collect_pay%type := '1';
  v_bank_account   prod_db.t_bank_text_detail.account%type;
  v_policy_id      prod_db.t_contract_master.policy_id%type;
  v_pay_mode       prod_db.t_bank_text_detail.pay_mode%type := '3';
  v_account_id     prod_db.t_bank_account.account_id%type;
  v_bank_code      prod_db.t_bank_code_cfg.bankcode%type;
  v_real_bank_code prod_db.t_bank_account.bank_code%type;
  v_tmp_bank_code  prod_db.t_bank_account.bank_code%type;
  v_fee_amount     prod_db.t_bank_text_detail.fee_amount%type := '%inputParameter_1%';
  v_acco_name      prod_db.t_bank_account.acco_name%type;
  v_certi_code     prod_db.t_bank_account.certi_code%type;
  v_detail_id      prod_db.t_bank_text_detail.detail_id%type;
  v_bank_branch    prod_db.t_bank_text_detail.bank_branch%type;
  v_fee_source     prod_db.t_bank_text_detail.fee_source%type := '1';
  v_purpose_type   prod_db.t_bank_text_detail.purpose_type%type := '1';
  v_organ_id       prod_db.t_bank_code_cfg.organ_id%type;
  v_master_chg_id  prod_db.t_bank_text_detail.master_chg_id%type;
  v_total_prem     prod_db.t_contract_product.total_prem_af%TYPE;
  v_due_date       prod_db.t_contract_extend.due_date%TYPE;
  v_count_rs       smallint;
BEGIN
  prod_db.PKG_PUB_APP_CONTEXT.P_SET_APP_USER_ID(316);

select tcm.policy_id
into v_policy_id
from prod_db.t_contract_master tcm
where tcm.policy_code = v_policy_code;

select tba.account_id,
       tba.bank_code,
       tba.branch_code,
       tba.bank_account,
       tba.acco_name,
       tc.certi_code
into v_account_id,
    v_tmp_bank_code,
    v_bank_branch,
    v_bank_account,
    v_acco_name,
    v_certi_code
from prod_db.t_bank_account tba, prod_db.t_payer_account tpa, prod_db.t_customer tc
where tba.account_id = tpa.next_account_id
  and tba.party_id = tc.customer_id
  and tpa.approval_status = '1'
  and tpa.payment_order = 1
  and tpa.policy_id = v_policy_id;

select t.bank_code
into v_real_bank_code
from prod_db.t_bank t
where bank_class = 1
    start with t.bank_code = v_tmp_bank_code
connect by prior t.parent_bank = t.bank_code;

select bankcode, organ_id
into v_bank_code, v_organ_id
from prod_db.t_bank_code_cfg
where paybank = v_real_bank_code
  and collectpay = v_collect_pay
  and transchannel = 1
  and account_type = 2
  and purposetype = v_purpose_type;

IF v_account_id IS NULL THEN
    dbms_output.put_line('account id not found!!!!---' || v_policy_id);
goto next_loop;
END IF;
  v_detail_id := prod_db.S_BANK_TEXT_DETAIL__DETAIL_ID.NEXTVAL;



insert into prod_db.t_bank_text_detail
(send_id,
 list_id,
 fee_id,
 pay_mode,
 account_id,
 bank_code,
 account,
 reference,
 fee_amount,
 acco_name,
 certi_code,
 organ_id,
 unsuccess_id,
 status,
 collect_pay,
 transfer_date,
 detail_id,
 policy_id,
 text_status,
 due_fee_id,
 other_rejection_reason,
 is_bankruptcy,
 is_generation_ltr,
 bank_branch,
 oi_list_id,
 deduction_date,
 fee_type,
 due_time,
 money_id,
 master_chg_id,
 row_index,
 fee_source,
 ebank_code,
 purpose_type,
 real_bank_code,
 has_checked,
 limit_code)
values
    (0,
     null,
     null,
     v_pay_mode,
     v_account_id,
     v_bank_code,
     v_bank_account,
     v_policy_code,
     v_fee_amount,
     v_acco_name,
     v_certi_code,
     v_organ_id,
     null,
     'N',
     v_collect_pay,
     sysdate,
     v_detail_id,
     v_policy_id,
     '1',
     null,
     null,
     null,
     null,
     v_bank_branch,
     null,
     trunc(sysdate),
     null,
     trunc(sysdate),
     '1',
     v_master_chg_id,
     null,
     v_fee_source,
     null,
     v_purpose_type,
     v_real_bank_code,
     null,
     v_detail_id);

COMMIT;
<<next_loop>>
  null;
end;
/
