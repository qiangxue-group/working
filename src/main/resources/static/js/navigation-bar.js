document.querySelectorAll('.dropdown').forEach(function(dropdown) {
    dropdown.addEventListener('mouseover', function() {
        this.querySelector('.dropdown-menu').style.display = 'block';
        this.classList.add('show');
    });
    dropdown.addEventListener('mouseout', function() {
        this.querySelector('.dropdown-menu').style.display = 'none';
        this.classList.remove('show');
    });
});