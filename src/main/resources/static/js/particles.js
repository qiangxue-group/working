function createParticle(x, y, colorClass) {
    const particle = document.createElement('div');
    particle.classList.add('particle', colorClass);
    document.body.appendChild(particle);
    const angle = Math.random() * 2 * Math.PI;
    const distance = Math.random() * 150 + 50; // 粒子飞行距离
    const xOffset = distance * Math.cos(angle);
    const yOffset = distance * Math.sin(angle);
    particle.style.setProperty('--x', `${xOffset}px`);
    particle.style.setProperty('--y', `${yOffset}px`);
    particle.style.left = `${x}px`;
    particle.style.top = `${y}px`;
    particle.addEventListener('animationend', () => {
        particle.remove();
    });
}

document.addEventListener('click', (e) => {
    const colors = ['red', 'green', 'blue', 'yellow'];
    colors.forEach(color => createParticle(e.clientX, e.clientY, color));
});