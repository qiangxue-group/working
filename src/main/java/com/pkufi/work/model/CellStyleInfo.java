package com.pkufi.work.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CellStyleInfo {
    private String fontName;
    private short fontSize;
    private boolean bold;
}
