package com.pkufi.work.mail.service.impl;

import com.pkufi.work.mail.service.SendEmailHandler;
import com.pkufi.work.mail.vo.EmailData;
import com.pkufi.work.util.UniversalTools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
@Slf4j
public class SendEmailHandlerImpl implements SendEmailHandler {

    private final JavaMailSender javaMailSender;

    @Autowired(required = false)
    public SendEmailHandlerImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendEMail(EmailData emailData, boolean existAttch) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");

            helper.setFrom(emailData.getFromAddr());
            // 设置收件人 用数组发送多个邮件
            if (emailData.getToList() != null) {
                if (!emailData.getToList().isEmpty()) {
                    String[] array = emailData.getToList().toArray(new String[] {});
                    helper.setTo(array);
                }
            }

            if (emailData.getCcList() != null) {
                if (!emailData.getCcList().isEmpty()) {
                    String[] array = emailData.getCcList().toArray(new String[] {});
                    helper.setCc(array);
                }
            }
            if (emailData.getBccList() != null) {
                if (!emailData.getBccList().isEmpty()) {
                    String[] array = emailData.getBccList().toArray(new String[] {});
                    helper.setBcc(array);
                }
            }

            if (emailData.getSubject() != null) {
                helper.setSubject(emailData.getSubject());
            }else{
                throw new Exception("subject is null!");
            }

            if (emailData.getContext() != null) {
                helper.setText(emailData.getContext(), true);
            } else {
                helper.setText("", true);
            }

            if(existAttch){
                if(null != emailData.getAttachmentPath()
                        && null != emailData.getAttachmentName() && !emailData.getAttachmentName().isEmpty()){

                    for (String attachmentName: emailData.getAttachmentName()) {
                        //添加附件
                        FileSystemResource file;
                        if(UniversalTools.isWin()){
                            file = new FileSystemResource(new File(emailData.getAttachmentPath()+"\\"+attachmentName));
                        }else{
                            file = new FileSystemResource(new File(emailData.getAttachmentPath()+"/"+attachmentName));
                        }
                        //String fileName = emailData.getAttachmentPath().substring(emailData.getAttachmentPath().lastIndexOf(File.separator));
                        helper.addAttachment(attachmentName, file);
                    }
                }
            }

            javaMailSender.send(mimeMessage);
            log.info("Send Email Successfully!");
        } catch (Exception e) {
            log.info("Send Email Filed!"+ e);
        }
    }
}