package com.pkufi.work.mail.service.impl;

import com.pkufi.work.mail.service.MailService;
import com.pkufi.work.mail.vo.Mail;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.search.*;
import java.util.*;

@Service
public class MailServiceImpl implements MailService {

    private final JavaMailSender javaMailSender;

    @Autowired(required = false)
    public MailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public List<Mail> getMails(String subjectToSearch) {
        List<Mail> mails = new ArrayList<>();
        try {
            JavaMailSenderImpl mailSenderImpl = (JavaMailSenderImpl) javaMailSender;
            //mailSenderImpl.setPort(993);
            Properties props = mailSenderImpl.getJavaMailProperties();
            Session session = Session.getInstance(props);

//            Store store = session.getStore("imap");
            Store store = session.getStore("pop3");
            store.connect(mailSenderImpl.getHost(), mailSenderImpl.getUsername(), mailSenderImpl.getPassword());

            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_ONLY);

            // 获取当天的开始时间和结束时间
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            Date startDate = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            Date endDate = calendar.getTime();

            // 创建日期条件
            SearchTerm dateTerm = new AndTerm(
                    new SentDateTerm(ComparisonTerm.GE, startDate),
                    new SentDateTerm(ComparisonTerm.LE, endDate)
            );

            // 创建主题条件
            SearchTerm subjectTerm = new SubjectTerm(subjectToSearch);

            // 组合日期条件和主题条件
            SearchTerm searchTerm = new AndTerm(dateTerm, subjectTerm);

            // 搜索邮件
            Message[] messages = inbox.search(searchTerm);

            System.out.println("Found " + messages.length + " messages with subject: " + subjectToSearch + " today.");

            for (Message message : messages) {
                String subject = message.getSubject();
                String from = StringUtil.join(", ", message.getFrom());
                //String date = message.getReceivedDate().toString();
                String content = message.getContent().toString();

                mails.add(new Mail(subject, from, content));
            }

            inbox.close(false);
            store.close();

        } catch (MessagingException e) {
            System.err.println("MessagingException: " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
            e.printStackTrace();
        }
        return mails;
    }
}