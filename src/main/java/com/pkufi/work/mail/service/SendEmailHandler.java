package com.pkufi.work.mail.service;

import com.pkufi.work.mail.vo.EmailData;

public interface SendEmailHandler {

    void sendEMail(EmailData emailData, boolean existAttch);
}
