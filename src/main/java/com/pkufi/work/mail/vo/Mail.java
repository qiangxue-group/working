package com.pkufi.work.mail.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Mail {
    private String subject;
    private String from;
//    private String date;
    private String content;

    // Constructor, getters, and setters

// Getters and Setters
}