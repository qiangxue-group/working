package com.pkufi.work.feecharge.repository;

import com.pkufi.work.feecharge.entity.CommonRowResult;

import java.util.List;

public interface RefundDataRepository {

    List<CommonRowResult> queryRefundData(Object... params);

}
