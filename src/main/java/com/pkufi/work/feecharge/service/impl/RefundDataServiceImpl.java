package com.pkufi.work.feecharge.service.impl;

import com.pkufi.work.feecharge.entity.CommonRowResult;
import com.pkufi.work.feecharge.repository.RefundDataRepository;
import com.pkufi.work.feecharge.service.RefundDataService;
import com.pkufi.work.mail.service.SendEmailHandler;
import com.pkufi.work.mail.vo.EmailData;
import com.pkufi.work.model.CellStyleInfo;
import com.pkufi.work.model.ExcelData;
import com.pkufi.work.util.ExcleGenerrator;
import com.pkufi.work.util.UniversalTools;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RefundDataServiceImpl implements RefundDataService {

    @Value("${fromMail.fromAddr}")
    private String fromAddr;

    private static final String subject = "退费数据";

    private final RefundDataRepository refundDataRepository;
    private final SendEmailHandler sendEmailHandler;

    @Override
    public List<CommonRowResult> getRefundDataByDate(String startDate, String endDate) {

        List<CommonRowResult> commonRowResults = refundDataRepository.queryRefundData(startDate, endDate);

        return commonRowResults;
    }

    /**
     * 根据指定的开始和结束日期，导出退款数据并发送电子邮件。
     * 
     * @param startDate 开始日期，用于筛选退款数据
     * @param endDate 结束日期，用于筛选退款数据
     * @return 返回导出操作的结果描述（目前返回空字符串）
     */
    @Override
    public String exportAndSendEmail(String startDate, String endDate){
    
        // 根据日期范围获取退款数据
        List<CommonRowResult> commonRowResults = getRefundDataByDate(startDate, endDate);
        
        // 构建Excel数据对象，设置标题和数据样式的默认样式
        // 将查询结果封装到 ExcelData 中
        ExcelData excelData = ExcelData.builder()
                .headerCellStyle(new CellStyleInfo("宋体", (short) 12,true))
                .dataCellStyle(new CellStyleInfo("宋体", (short) 11,false))
                .build();
    
        // 如果查询结果不为空
        if (!commonRowResults.isEmpty()) {
            // 从查询结果中获取第一行数据，用于提取Excel的表头
            CommonRowResult firstRow = commonRowResults.get(0);
            // 从第一行数据中提取表头
            List<String> headerRow = new ArrayList<>(firstRow.getRowMap().keySet());
            // 设置Excel的表头
            excelData.setHeaderRow(headerRow);
    
            // 初始化数据行列表
            List<List<String>> dataRows = new ArrayList<>();
            // 遍历所有查询结果，构建数据行列表
            for (CommonRowResult row : commonRowResults) {
                // 初始化一行数据的列表
                List<String> dataRow = new ArrayList<>();
                // 将每行数据的值转换为字符串，添加到数据行列表
                for (Object value : row.getRowMap().values()) {
                    dataRow.add(value != null ? value.toString() : "");
                }
                // 将数据行添加到数据行列表
                dataRows.add(dataRow);
            }
            // 设置Excel的数据行
            excelData.setDataRows(dataRows);
        }
    
        // 构建Excel文件名
        String fileName = startDate + "-" + endDate + ".xlsx";
        // 构建Excel文件路径
        String filePath = "downloads/" + fileName;
        // 生成Excel文件
        String fullFilePath = ExcleGenerrator.generateExcel(filePath, excelData);
    
        // 构建邮件数据对象
        // 邮件发送
        EmailData emailData = EmailData.builder()
                .fromAddr(fromAddr)
                .toList(new ArrayList<>() {{
                    this.add("zac_sun@pkufi.com");
                    this.add("zuozheng_zhang@pkufi.com");
                }})
                .ccList(new ArrayList<>() {{
                    this.add("joy_tan@pkufi.com");
                }})
                .bccList(new ArrayList<>() {{
                    this.add("qiang_xue@pkufi.com");
                }})
                .subject(startDate + "-" + endDate + subject)
                .context("<html>" +
                        "    <body>" +
                        "        Dear <br/>" +
                        "        &nbsp&nbsp&nbsp&nbsp请查收，谢谢。<br/>" +
                        "        <br/>" +
                        "        <br/>" +
                        "        薛强  Qiang xue" +
                        "    </body>" +
                        "</html>")
                .attachmentName(new ArrayList<>() {{
                    this.add(fileName);
                }})
                .build();
    
        // 根据操作系统类型，处理附件路径
        // 使用 replace 方法
        String resultString = fullFilePath.replace(fileName, "");
    
        // 判断是否为Windows系统，去除路径中的多余斜杠
        if(UniversalTools.isWin()){
            // 去掉可能多余的斜杠
            if (resultString.endsWith("\\")) {
                resultString = resultString.substring(0, resultString.length() - 1);
            }
            emailData.setAttachmentPath(resultString);
        }else{
            // 去掉可能多余的斜杠
            if (resultString.endsWith("/")) {
                resultString = resultString.substring(0, resultString.length() - 1);
            }
            emailData.setAttachmentPath(resultString);
        }
    
        // 发送邮件
        sendEmailHandler.sendEMail(emailData, true);
    
        // 返回空字符串
        return "";
    }
}
