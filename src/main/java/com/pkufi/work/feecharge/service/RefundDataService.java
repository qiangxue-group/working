package com.pkufi.work.feecharge.service;

import com.pkufi.work.feecharge.entity.CommonRowResult;

import java.util.List;

public interface RefundDataService {
    List<CommonRowResult> getRefundDataByDate(String startDate, String endDate);
    String exportAndSendEmail(String startDate, String endDate);
}
