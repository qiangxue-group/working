package com.pkufi.work.feecharge.controller;

import com.pkufi.work.feecharge.service.FeesAndChargesService;
import com.pkufi.work.service.UploadFileService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
@AllArgsConstructor
public class BatchAdjustController {

    private static final String UPLOAD_DIRECTORY = "/Users/xueqiang/Library/CloudStorage/OneDrive-个人/win桌面/脚本/收付费/批量加扣";

    private final FeesAndChargesService feesAndChargesService;
    private final UploadFileService uploadFileService;

    //这种方式是在页面处理csrfToken的，页面取到默认的令牌，在ajax提交的时候带上令牌请求后台（batchAdjust.html中js注释部分）
    @GetMapping("/batchAdjust")
    public String showBatchAdjustPage() {
        return "fee/batchAdjust";
    }

    //使用页面初始化是拿到csrfToken放到model，这样需要页面通过隐藏表单获取到模型中的令牌，表单提交是会提交给Security(batchAdjust.htm表单注释部分)
//    @GetMapping("/batchAdjust")
//    public String showBatchAdjustPage(CsrfToken csrfToken, Model model) {
//        model.addAttribute("_csrf", csrfToken);
//        return "fee/batchAdjust";
//    }

    @PostMapping("/upload")
    public ResponseEntity<Map<String, String>> handleFileUpload(@RequestParam("file") MultipartFile file) {
        Map<String, String> response = new HashMap<>();
        if (file.isEmpty()) {
            response.put("message", "文件为空");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            String msg = uploadFileService.uploadFile(file, UPLOAD_DIRECTORY);
            response.put("message", msg);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            response.put("message", "文件上传失败");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/batchAdjustSubmit")
    @ResponseBody
    public String batchAdjustSubmit(@RequestParam("filePath") String filePath) throws Exception {
        // 处理提交请求，参数为上传成功后的文件路径
        // 这里可以添加具体的处理逻辑
        feesAndChargesService.executeBatchAddFees(filePath);
        return "批量加扣邮件已发送！";
    }
}