package com.pkufi.work.util;

import com.pkufi.work.annotation.ParamDescription;
import com.pkufi.work.model.CellStyleInfo;
import com.pkufi.work.model.ExcelData;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;

@Slf4j
@Component
public class ExcleGenerrator {
    public static String generateExcel(@ParamDescription("项目路径/本地磁盘路径")String savePath, ExcelData excelData) {
        // 创建工作簿
        Workbook workbook = new XSSFWorkbook();

        // 创建工作表
        Sheet sheet = workbook.createSheet("Sheet1");

        // 创建标题行样式
        CellStyle headerCellStyle = createCellStyle(workbook, excelData.getHeaderCellStyle());

        // 创建数据行样式
        CellStyle dataCellStyle = createCellStyle(workbook, excelData.getDataCellStyle());

        // 写入标题行
        log.info("Writing header row...");
        Row headerRow = sheet.createRow(0);
        int headerCellIndex = 0;
        for (String headerCellData : excelData.getHeaderRow()) {
            Cell headerCell = headerRow.createCell(headerCellIndex++);
            headerCell.setCellValue(headerCellData);
            headerCell.setCellStyle(headerCellStyle);
        }

        // 写入数据行
        log.info("Writing data rows...");
        int dataRowIndex = 1;
        for (List<String> dataRow : excelData.getDataRows()) {
            Row row = sheet.createRow(dataRowIndex++);
            int dataCellIndex = 0;
            for (String dataCellData : dataRow) {
                Cell cell = row.createCell(dataCellIndex++);
                cell.setCellValue(dataCellData);
                cell.setCellStyle(dataCellStyle);
            }
        }
        // 获取完整保存路径
        String fullSavePath = getFullPath(savePath);
        log.info("Saving Excel file to: " + fullSavePath);
        // 保存文件
        try (FileOutputStream fileOutputStream = new FileOutputStream(fullSavePath)) {
            workbook.write(fileOutputStream);
            log.info("Excel file saved successfully.");
        } catch (IOException e) {
            log.error("Error saving Excel file: " + e.getMessage());
            e.printStackTrace();
        }

        // 关闭工作簿
        try {
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fullSavePath;
    }

    private static String getFullPath(String savePath) {
        // 获取完整的保存路径
        String fullSavePath;
        if (Paths.get(savePath).isAbsolute()) {
            // 绝对路径
            fullSavePath = savePath;
        } else {
            // 相对路径，基于项目路径
            String projectPath = System.getProperty("user.dir");
            fullSavePath = Paths.get(projectPath, savePath).toString();
        }
        return fullSavePath;
    }

    public static void main(String[] args) {

        // 创建数据对象
        List<String> headerRow = List.of("保单号", "投保人", "证件号码");
        List<List<String>> dataRows = List.of(
                List.of("29293839392", "小明", "234512342122318893"),
                List.of("34212131233", "大明", "341123112443256786")
        );
        ExcelData excelData = ExcelData.builder()
                .headerRow(headerRow)
                .dataRows(dataRows)
                .headerCellStyle(new CellStyleInfo("宋体", (short) 12,true))
                .dataCellStyle(new CellStyleInfo("等线", (short) 11,false))
                .build();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String fileName = df.format(System.currentTimeMillis()) + "_error.xlsx";
        System.out.println(fileName);

        ClassLoader classLoader = excelData.getClass().getClassLoader();
        URL resourceURL = classLoader.getResource("temp");
        System.out.println(resourceURL);
        // 生成 Excel 文件
        //generateExcel(path+"/sample.xlsx", excelData);
    }

    private static CellStyle createCellStyle(Workbook workbook, CellStyleInfo cellStyleInfo) {
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName(cellStyleInfo.getFontName());
        font.setFontHeightInPoints(cellStyleInfo.getFontSize());
        font.setBold(cellStyleInfo.isBold());
        cellStyle.setFont(font);
        return cellStyle;
    }
}
