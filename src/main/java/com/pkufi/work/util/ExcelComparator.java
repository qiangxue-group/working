package com.pkufi.work.util;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelComparator {

    public static List<String[]> compareExcelFiles(String fileAPath, String fileBPath) {
        List<String[]> chayiList = new ArrayList<>();
        List<String> bucunzaiList = new ArrayList<>();

        try {
            FileInputStream fileAInput = new FileInputStream(fileAPath);
            FileInputStream fileBInput = new FileInputStream(fileBPath);

            Workbook workbookA = new XSSFWorkbook(fileAInput);
            Workbook workbookB = new XSSFWorkbook(fileBInput);

            Sheet sheetA = workbookA.getSheetAt(0);
            Sheet sheetB = workbookB.getSheetAt(0);

            int rowCountA = sheetA.getLastRowNum() + 1;
            int rowCountB = sheetB.getLastRowNum() + 1;

            for (int i = 0; i < rowCountA; i++) {
                Row rowA = sheetA.getRow(i);
                Cell cellA = rowA.getCell(0);
                String valueA;

                if (cellA.getCellType() == CellType.STRING) {
                    valueA = cellA.getStringCellValue();
                } else if (cellA.getCellType() == CellType.NUMERIC) {
                    // Handle numeric cell as string
                    valueA = String.valueOf(cellA.getNumericCellValue());
                } else {
                    // Handle other cell types if needed
                    valueA = ""; // or handle it differently
                }

                boolean found = false;

                for (int j = 0; j < rowCountB; j++) {
                    Row rowB = sheetB.getRow(j);
                    Cell cellB = rowB.getCell(0);
                    String valueB;

                    if (cellB.getCellType() == CellType.STRING) {
                        valueB = cellB.getStringCellValue();
                    } else if (cellB.getCellType() == CellType.NUMERIC) {
                        // Handle numeric cell as string
                        valueB = String.valueOf(cellB.getNumericCellValue());
                    } else {
                        // Handle other cell types if needed
                        valueB = ""; // or handle it differently
                    }

                    if (valueA.equals(valueB)) {
                        Cell cellB2 = rowB.getCell(1);
                        Cell cellB3 = rowB.getCell(2);
                        String valueB2;
                        if (cellB2.getCellType() == CellType.STRING) {
                            valueB2 = cellB2.getStringCellValue();
                        } else if (cellB2.getCellType() == CellType.NUMERIC) {
                            // Handle numeric cell as string
                            valueB2 = String.valueOf(cellB2.getNumericCellValue());
                        } else {
                            // Handle other cell types if needed
                            valueB2 = ""; // or handle it differently
                        }

                        String valueB3;
                        if (cellB3.getCellType() == CellType.STRING) {
                            valueB3 = cellB3.getStringCellValue();
                        } else if (cellB3.getCellType() == CellType.NUMERIC) {
                            // Handle numeric cell as string
                            valueB3 = String.valueOf(cellB3.getNumericCellValue());
                        } else {
                            // Handle other cell types if needed
                            valueB3 = ""; // or handle it differently
                        }
                        Cell cellA2 = rowA.getCell(1);
                        Cell cellA3 = rowA.getCell(2);
                        String valueA2;
                        if (cellA2.getCellType() == CellType.STRING) {
                            valueA2 = cellA2.getStringCellValue();
                        } else if (cellA2.getCellType() == CellType.NUMERIC) {
                            // Handle numeric cell as string
                            valueA2 = String.valueOf(cellA2.getNumericCellValue());
                        } else {
                            // Handle other cell types if needed
                            valueA2 = ""; // or handle it differently
                        }
                        String valueA3 ;
                        if(cellA3==null){
                            valueA3 = "";
                        }else{
                            if (cellA3.getCellType() == CellType.STRING) {
                                valueA3 = cellA3.getStringCellValue();
                            } else if (cellA3.getCellType() == CellType.NUMERIC) {
                                // Handle numeric cell as string
                                valueA3 = String.valueOf(cellA3.getNumericCellValue());
                            } else {
                                // Handle other cell types if needed
                                valueA3 = ""; // or handle it differently
                            }
                        }

                        if (!valueA2.equals(valueB2) || !valueA3.equals(valueB3)) {
                            String[] diffRow = { valueA, valueA2, valueA3, valueB, valueB2, valueB3 };
                            chayiList.add(diffRow);
                        }

                        found = true;
                        break;
                    }
                }

                if (!found&&i!=0) {
                    bucunzaiList.add(valueA);
                }
            }

            workbookA.close();
            workbookB.close();

            fileAInput.close();
            fileBInput.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        //System.out.println("chayiList: " + chayiList);
        chayiList.forEach(diffRow -> System.out.println(String.join("|", diffRow)));
        System.out.println("bucunzaiList: " + bucunzaiList);

        return chayiList;
    }

    public static void main(String[] args) {
        String fileAPath = "/Users/xueqiang/Library/CloudStorage/OneDrive-个人/temp/工作/月结借贷方对比/202404/个险4月批次.xlsx";
        String fileBPath = "/Users/xueqiang/Library/CloudStorage/OneDrive-个人/temp/工作/月结借贷方对比/202404/CAP-总账.xlsx";

        compareExcelFiles(fileAPath, fileBPath);
    }
}
