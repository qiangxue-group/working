package com.pkufi.work.util;

public class UniversalTools {

    public static boolean isWin(){
        String os = System.getProperty("os.name").toLowerCase();

        return os.contains("win");
    }
}
