package com.pkufi.work.util;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;


/**
 * @Description: 读取excel返回数据文本
 * @Author: qiang_xue
 * @date: 2023/3/13 10:04
 * @package: com.xue.working.util
 * @Version: 1.0
 */
@Component
public class GenerateSqlWithExcel {


    public String readExcelToSql(String excelPath){

        StringBuilder sb = new StringBuilder();
        int loopNum = 6;
        try {
            // 获取文件输入流
            InputStream inputStream = Files.newInputStream(Paths.get(excelPath));
            // 定义一个org.apache.poi.ss.usermodel.Workbook的变量
            //xls使用HSSFWorkbook，xlsx使用XSSFWorkbook
            Workbook workbook = new XSSFWorkbook(inputStream);
            // 获取第一张表
            Sheet sheet = workbook.getSheetAt(0);
            sb.append("delete from prod_db.v_temporary_pay_20220720;\r\n");
            // sheet.getPhysicalNumberOfRows()获取总的行数
            // 循环读取每一行(从第二行开始)
            boolean flag = false;
            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                if(flag){
                    break;
                }
                // 循环读取每一个格
                Row row = sheet.getRow(i);
                // row.getPhysicalNumberOfCells()获取总的列数(从第二列开始 到 倒数第二列)

                for (int index = 1; index < loopNum-1; index++) {
                    // 获取数据，但是我们获取的cell类型
                    Cell cell = row.getCell(index);
                    if(cell==null){
                        continue;
                    }
                    // 转换为字符串类型
                    cell.setCellType(CellType.STRING);
                    // 获取得到字符串,去除空格（包含普通空格、NBSP格式空格）
                    String str = cell.getStringCellValue().trim().replaceAll("(\\u00A0+| )","");
                    if(str.trim().isEmpty()){
                        flag = true;
                        break;
                    }
                    sb.append("parameter_").append(index);
                    sb.append(str.trim());
                }
                if(!flag){
                    sb.append(");\r\n");
                }
            }
            sb.append("commit;");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        String sql = sb.toString();

        sql = sql.replaceAll("parameter_1", "insert into prod_db.v_temporary_pay_20220720 (POLICY_CODE, FEE_AMOUNT, BANK_ACCOUNT, ACCOUNT_ID) values ('");
        sql = sql.replaceAll("parameter_2", "',");
        sql = sql.replaceAll("parameter_3", ",'");
        sql = sql.replaceAll("parameter_4", "',");
        sql = sql.replaceAll("parameter_5", ");");

        return sql;
    }
}
