package com.pkufi.work.util;

public class ConverterUtils {
    /**
     * 驼峰转为下划线
     */
    public static String convertToUnderscore(String input) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);

            // 判断当前字符是否为大写字母
            if (Character.isUpperCase(currentChar)) {
                // 如果不是首字母，则在前面添加下划线
                if (i > 0) {
                    result.append('_');
                }

                // 将大写字母转为小写
                result.append(Character.toLowerCase(currentChar));
            } else {
                result.append(currentChar);
            }
        }

        return result.toString();
    }

    /**
     * 下划线转驼峰
     */
    public static String convertToCamelCase(String input) {
        StringBuilder result = new StringBuilder();

        // 标记下一个字符是否为大写
        boolean nextUpperCase = false;

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);

            if (currentChar == '_') {
                nextUpperCase = true;
            } else {
                if (nextUpperCase) {
                    result.append(Character.toUpperCase(currentChar));
                    nextUpperCase = false;
                } else {
                    result.append(Character.toLowerCase(currentChar));
                }
            }
        }

        return result.toString();
    }
}
