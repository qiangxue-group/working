package com.pkufi.work.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    @GetMapping("/finmodule")
    public String finModule() {
        return "fin/finmodule";  // 返回 finmodule.html 模板
    }

    @GetMapping("/feemodule")
    public String feeModule() {
        return "fee/feemodule";  // 返回 feemodule.html 模板
    }

    @GetMapping("/thirdmodule")
    public String thirdModule() {
        return "thirdmodule";  // 返回 thirdmodule.html 模板
    }
}
