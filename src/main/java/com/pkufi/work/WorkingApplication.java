package com.pkufi.work;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class WorkingApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkingApplication.class, args);
    }

}
