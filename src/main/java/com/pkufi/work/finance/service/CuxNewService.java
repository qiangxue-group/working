package com.pkufi.work.finance.service;


import com.pkufi.work.finance.entity.CuxNew;

import java.util.List;

public interface CuxNewService {
    List<CuxNew> queryPendingStatus();

    List<CuxNew> queryErrorStatus();

    List<CuxNew> queryErrorInfo(String str);
}
