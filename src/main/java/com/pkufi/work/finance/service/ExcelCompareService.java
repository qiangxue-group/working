package com.pkufi.work.finance.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

public interface ExcelCompareService {

    String compareExcelFiles(String file1Path, String file2Path);

    Map<String, Object> compareExcelToPage(String file1Path, String file2Path);

    Map<String, Object> handleFileUpload(MultipartFile file, String folderPath);

    String generateSecondFile(String filePath) throws IOException;
}
