package com.pkufi.work.finance.service.impl;
import com.pkufi.work.feecharge.entity.CommonRowResult;
import com.pkufi.work.finance.repository.BookkeptSumRepository;
import com.pkufi.work.finance.service.ExcelCompareService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExcelCompareServiceImpl implements ExcelCompareService {

    @Value("${file.upload-dir}")
    private String uploadDir;

    private final BookkeptSumRepository bookkeptSumRepository;

    public ExcelCompareServiceImpl(BookkeptSumRepository bookkeptSumRepository) {
        this.bookkeptSumRepository = bookkeptSumRepository;
    }

    @Override
    public String compareExcelFiles(String file1Path, String file2Path) {
        String outputPath = generateOutputPath(file2Path);

        try {
            Map<String, Amounts> table1Data = readExcel(file1Path);
            Map<String, Amounts> table2Data = readExcel(file2Path);

            Workbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Differences");

            int rowCount = 0;

            Row header = sheet.createRow(rowCount++);
            header.createCell(0).setCellValue("source_batch_id");
            header.createCell(1).setCellValue("debit_amount_1");
            header.createCell(2).setCellValue("credit_amount_1");
            header.createCell(3).setCellValue("debit_amount_2");
            header.createCell(4).setCellValue("credit_amount_2");

            for (String batchId : table1Data.keySet()) {
                Amounts amounts1 = table1Data.get(batchId);
                Amounts amounts2 = table2Data.getOrDefault(batchId, new Amounts(0, 0));

                if (!amounts1.equals(amounts2)) {
                    Row row = sheet.createRow(rowCount++);
                    row.createCell(0).setCellValue(batchId);
                    row.createCell(1).setCellValue(amounts1.debitAmount);
                    row.createCell(2).setCellValue(amounts1.creditAmount);
                    row.createCell(3).setCellValue(amounts2.debitAmount);
                    row.createCell(4).setCellValue(amounts2.creditAmount);
                }
            }

            for (String batchId : table2Data.keySet()) {
                if (!table1Data.containsKey(batchId)) {
                    Amounts amounts1 = new Amounts(0, 0);
                    Amounts amounts2 = table2Data.get(batchId);

                    Row row = sheet.createRow(rowCount++);
                    row.createCell(0).setCellValue(batchId);
                    row.createCell(1).setCellValue(amounts1.debitAmount);
                    row.createCell(2).setCellValue(amounts1.creditAmount);
                    row.createCell(3).setCellValue(amounts2.debitAmount);
                    row.createCell(4).setCellValue(amounts2.creditAmount);
                }
            }

            try (FileOutputStream outputStream = new FileOutputStream(outputPath)) {
                workbook.write(outputStream);
            }
            workbook.close();

            System.out.println("Differences written to " + outputPath);
            return outputPath;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String generateOutputPath(String file2Path) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String date = dateFormat.format(new Date());
        File file = new File(file2Path);
        String parentDir = file.getParent();
        String baseName = file.getName().replaceFirst("[.][^.]+$", "");
        return parentDir + File.separator + baseName + "_differences_" + date + ".xlsx";
    }

    private Map<String, Amounts> readExcel(String filePath) throws IOException {
        Map<String, Amounts> data = new HashMap<>();

        try (FileInputStream file = new FileInputStream(filePath);
             Workbook workbook = new XSSFWorkbook(file)) {
            Sheet sheet = workbook.getSheetAt(0);
            for (Row row : sheet) {
                if (row.getRowNum() == 0) continue;

                Cell cell1 = row.getCell(0);
                Cell cell2 = row.getCell(1);
                Cell cell3 = row.getCell(2);

                String sourceBatchId = cell1.getStringCellValue();
                double debitAmount = cell2.getNumericCellValue();
                double creditAmount = cell3.getNumericCellValue();

                data.put(sourceBatchId, new Amounts(debitAmount, creditAmount));
            }
        }

        return data;
    }

    static class Amounts {
        double debitAmount;
        double creditAmount;

        Amounts(double debitAmount, double creditAmount) {
            this.debitAmount = debitAmount;
            this.creditAmount = creditAmount;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Amounts amounts = (Amounts) o;

            if (Double.compare(amounts.debitAmount, debitAmount) != 0) return false;
            return Double.compare(amounts.creditAmount, creditAmount) == 0;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            temp = Double.doubleToLongBits(debitAmount);
            result = (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(creditAmount);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }

    private List<Map<String, Object>> readExcelToList(String filePath, int cell1, int cell2, int cell3) {
        List<Map<String, Object>> dataList = new ArrayList<>();

        try (FileInputStream fis = new FileInputStream(filePath);
             Workbook workbook = new XSSFWorkbook(fis)) {

            Sheet sheet = workbook.getSheetAt(0);

            // 读取数据
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                Map<String, Object> data = new HashMap<>();

                data.put("batchId", getCellValue(row.getCell(cell1))); // 第一列
                data.put("sum(DR)", getCellValue(row.getCell(cell2))); // 第二列
                data.put("sum(CR)", getCellValue(row.getCell(cell3))); // 第三列

                dataList.add(data);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return dataList;
    }

    private Object getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue();
                } else {
                    return cell.getNumericCellValue();
                }
            case BOOLEAN:
                return cell.getBooleanCellValue();
            case FORMULA:
                return cell.getCellFormula();
            case BLANK:
                return "";
            default:
                return "";
        }
    }

    @Override
    public Map<String, Object> compareExcelToPage(String file1Path, String file2Path) {
        // 读取两个Excel文件内容
        List<Map<String, Object>> file1Data = readExcelToList(file1Path,1,3,4);
        List<Map<String, Object>> file2Data = readExcelToList(file2Path,0,1,2);

        // 将file1Data和file2Data转换为Map，键为batchId，值为Map数据
        Map<String, Map<String, Object>> file1DataMap = file1Data.stream()
                .collect(Collectors.toMap(data -> data.get("batchId").toString(), data -> data));

        Map<String, Map<String, Object>> file2DataMap = file2Data.stream()
                .collect(Collectors.toMap(data -> data.get("batchId").toString(), data -> data));

        // 存储比较结果
        Map<String, Object> result = new HashMap<>();

        // 找出file1中存在但file2中缺少的batchId
        Set<String> file1BatchIds = file1DataMap.keySet();
        Set<String> file2BatchIds = file2DataMap.keySet();

        Set<String> missingInFile2 = file1BatchIds.stream()
                .filter(id -> !file2BatchIds.contains(id))
                .collect(Collectors.toSet());

        result.put("missingInFile2", missingInFile2);

        // 找出file2中存在但file1中缺少的batchId
        Set<String> missingInFile1 = file2BatchIds.stream()
                .filter(id -> !file1BatchIds.contains(id))
                .collect(Collectors.toSet());

        result.put("missingInFile1", missingInFile1);

        // 找出相同batchId但借贷金额存在差异的数据
        Map<String, Map<String, Object>> amountDifferences = new HashMap<>();
        for (String batchId : file1BatchIds) {
            if (file2BatchIds.contains(batchId)) {
                Map<String, Object> file1Record = file1DataMap.get(batchId);
                Map<String, Object> file2Record = file2DataMap.get(batchId);

                // 获取借贷金额
                double file1Debit = Double.parseDouble(file1Record.get("sum(DR)").toString());
                double file2Debit = Double.parseDouble(file2Record.get("sum(DR)").toString());
                double file1Credit = Double.parseDouble(file1Record.get("sum(CR)").toString());
                double file2Credit = Double.parseDouble(file2Record.get("sum(CR)").toString());

                if (file1Debit != file2Debit || file1Credit != file2Credit) {
                    Map<String, Object> difference = new HashMap<>();
                    difference.put("file1", file1Record);
                    difference.put("file2", file2Record);
                    amountDifferences.put(batchId, difference);
                }
            }
        }

        result.put("amountDifferences", amountDifferences);

        return result;
    }

    @Override
    public Map<String, Object> handleFileUpload(MultipartFile file, String folderPath) {
        Map<String, Object> response = new HashMap<>();
        try {
            File folder = new File(folderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            String filePath = folderPath + "/" + file.getOriginalFilename();
            Files.write(Paths.get(filePath), file.getBytes());

            // 生成第二个文件
            String secondFilePath = generateSecondFile(filePath);

            response.put("success", true);
            response.put("filePath", filePath);
            response.put("secondFilePath", secondFilePath);
        } catch (IOException e) {
            response.put("success", false);
            response.put("message", e.getMessage());
        }
        return response;
    }

    @Override
    public String generateSecondFile(String filePath) throws IOException {
        Workbook workbook = WorkbookFactory.create(new File(filePath));
        Sheet sheet = workbook.getSheetAt(0);
        StringBuilder sb = new StringBuilder();
        //List<String> list = new ArrayList<>();
        sb.append("'");
        for (Row row : sheet) {
            if (row.getRowNum() == 0) continue; // 跳过标题行
            Cell cell = row.getCell(1); // 获取第二列
            //list.add(cell.getStringCellValue());
            sb.append(cell.getStringCellValue()).append(",");
        }

        String queryString = sb.toString().replaceAll(",$", "'").replaceAll(",", "','"); // 去掉最后一个逗号


        List<CommonRowResult> dbResults = bookkeptSumRepository.queryBookkeptSumData(queryString);
        // 创建新的Excel文件
        Workbook newWorkbook = new XSSFWorkbook();
        Sheet newSheet = newWorkbook.createSheet("总账");

        // Write the header row
        if (!dbResults.isEmpty()) {
            CommonRowResult headerRowResult = dbResults.get(0);
            Row headerRow = sheet.createRow(0);
            int headerCellIndex = 0;
            for (String key : headerRowResult.getRowMap().keySet()) {
                Cell cell = headerRow.createCell(headerCellIndex++);
                cell.setCellValue(key);
            }

            // Write the data rows
            int rowIndex = 1;
            for (CommonRowResult result : dbResults) {
                Row row = sheet.createRow(rowIndex++);
                int cellIndex = 0;
                for (String key : headerRowResult.getRowMap().keySet()) {
                    Cell cell = row.createCell(cellIndex++);
                    Object value = result.get(key);
                    if (value != null) {
                        cell.setCellValue(value.toString());
                    } else {
                        cell.setCellValue("");
                    }
                }
            }
        }

        String secondFilePath = filePath.replace(".xlsx", "-总账.xlsx");
        try (FileOutputStream fileOut = new FileOutputStream(secondFilePath)) {
            newWorkbook.write(fileOut);
        } finally {
            newWorkbook.close();
            workbook.close();
        }

        return secondFilePath;
    }

    private List<String[]> queryDatabase(String queryString) {
        // 这里应该实现查询数据库的逻辑，返回一个包含三列数据的List
        // 暂时用一些模拟数据来代替
        List<String[]> results = new ArrayList<>();
        results.add(new String[]{"A", "B", "C"});
        results.add(new String[]{"D", "E", "F"});
        // ...
        return results;
    }

//    public Map<String, Object> compareExcelToPage(String file1Path, String file2Path) {
//        // 实现比较逻辑
//        Map<String, Object> result = new HashMap<>();
//        result.put("missingInFile2", new String[]{"example1", "example2"});
//        result.put("missingInFile1", new String[]{"example3", "example4"});
//        result.put("amountDifferences", new String[]{"example5", "example6"});
//        return result;
//    }
}