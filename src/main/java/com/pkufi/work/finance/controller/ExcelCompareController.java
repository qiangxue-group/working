package com.pkufi.work.finance.controller;

import com.pkufi.work.finance.service.ExcelCompareService;
import com.pkufi.work.service.impl.UploadFileServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class ExcelCompareController {

    @Value("${file.upload-dir}")
    private String uploadDir;

    private final ExcelCompareService excelCompareService;
    private final UploadFileServiceImpl uploadFileService;

    @GetMapping("/excelComparison")
    public String excelComparison() {
        return "fin/excelcomparison";
    }

    @GetMapping("/compareExcel")
    public String compareExcel(@RequestParam String file1Path, @RequestParam String file2Path, Model model) {
        String result = excelCompareService.compareExcelFiles(file1Path, file2Path);
        model.addAttribute("result", result != null ? "Differences written to: " + result : "Error processing files.");
        return "result";
    }

    /**
     * 用于页面展示结果
     */
    @PostMapping("/compareExcelToPage")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> compareExcelToPage(@RequestParam("file1") MultipartFile file1,
        @RequestParam(value = "file2", required = false) MultipartFile file2) {

        Map<String, Object> result = new HashMap<>();

        LocalDate now = LocalDate.now();

        // 获取当前月份的上一个月
        LocalDate previousMonth = now.minusMonths(1);

        // 获取年份和月份
        //String year = String.valueOf(previousMonth.getYear());
        //String month = previousMonth.format(DateTimeFormatter.ofPattern("MM"));

        //System.out.println("当前年: " + year);
        //System.out.println("上一个月: " + month);
        //previousMonth = LocalDate.parse(year + "-" + month + "-01", DateTimeFormatter.ofPattern("yyyy-MM-dd")).minusMonths(1);
        String folderPath = uploadDir + previousMonth.format(DateTimeFormatter.ofPattern("yyyyMM"));

        try {
            if (null!=file1&&null!=file2) {
                String file1Path = uploadFileService.uploadFile(file1, folderPath);
                String file2Path = uploadFileService.uploadFile(file2, folderPath);
                result = excelCompareService.compareExcelToPage(file1Path, file2Path);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }else if(null!=file1){
                //用1文件批次查过账数据
                String file1Path = uploadFileService.uploadFile(file1, folderPath);
                String file2Path = excelCompareService.generateSecondFile(file1Path);
                result = excelCompareService.compareExcelToPage(file1Path, file2Path);
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        } catch (Exception e) {
            result.put("error", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
