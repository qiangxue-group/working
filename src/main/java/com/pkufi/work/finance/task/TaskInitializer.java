package com.pkufi.work.finance.task;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class TaskInitializer {

    private final CheckTask checkTask;

    @PostConstruct
    public void init() {
        // 在应用启动后，手动触发定时任务的执行
        //checkTask.entryCheckScheduledTask();
    }
}
