package com.pkufi.work.finance.repository;


import com.pkufi.work.finance.entity.CuxNew;

import java.util.List;

public interface CuxNewRepository {

    List<CuxNew> queryPendingStatus();

    List<CuxNew> queryErrorStatus();

    List<CuxNew> queryErrorInfo(String str);
}
