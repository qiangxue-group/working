package com.pkufi.work.finance.repository;

import com.pkufi.work.feecharge.entity.CommonRowResult;

import java.util.List;

public interface BookkeptSumRepository {
    List<CommonRowResult> queryBookkeptSumData(List<String> params);

    List<CommonRowResult> queryBookkeptSumData(String params);
}
