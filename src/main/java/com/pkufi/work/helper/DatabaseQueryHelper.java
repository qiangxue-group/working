package com.pkufi.work.helper;

import com.pkufi.work.feecharge.entity.CommonRowResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Slf4j
@Component
public class DatabaseQueryHelper {

    private final JdbcTemplate primaryJdbcTemplate;
    private final JdbcTemplate namedParameterJdbcTemplate;

    private final JdbcTemplate secondaryJdbcTemplate;

    public DatabaseQueryHelper(@Qualifier("primaryJdbcTemplate") JdbcTemplate primaryJdbcTemplate, @Qualifier("secondaryJdbcTemplate") JdbcTemplate secondaryJdbcTemplate, @Qualifier("primaryJdbcTemplate")JdbcTemplate namedParameterJdbcTemplate) {
        this.primaryJdbcTemplate = primaryJdbcTemplate;
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.secondaryJdbcTemplate = secondaryJdbcTemplate;
    }

    /**
     * 无参查询
     */
    public <T> List<T> queryForEntities(String sql, Class<T> entityClass) {
        return primaryJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(entityClass));
    }

    /**
     * 带参查询
     */
    public <T> List<T> queryForEntities(String sql, Class<T> entityClass, Object... params) {

        log.info("Executing SQL: {}", getCompleteSql(sql, params));

        return namedParameterJdbcTemplate.query(sql, ps -> {
            int index = 1;
            for (Object param : params) {
                ps.setObject(index, param);
                index++;
            }
        }, new BeanPropertyRowMapper<>(entityClass));
    }

    /**
     * 打印脚本是，拼接脚本+参数
     */
    private String getCompleteSql(String sql, Object... params) {
        StringBuilder completeSql = new StringBuilder(sql);
        for (Object param : params) {
            completeSql.append(" [").append(param).append("]");
        }
        return completeSql.toString();
    }
}
