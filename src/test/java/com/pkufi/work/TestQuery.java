package com.pkufi.work;


import com.pkufi.work.finance.entity.CuxNew;
import com.pkufi.work.finance.service.CuxNewService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest(classes = WorkingApplication.class)
public class TestQuery {

    @Autowired
    private CuxNewService cuxNewService;
    @Test
    public void testQueryCheckStatus() {
        // 在测试方法中使用容器
        // 这里可以进行其他测试逻辑
        List<CuxNew> list = cuxNewService.queryPendingStatus();
        for (CuxNew cuxNew: list) {
            System.out.println(cuxNew.getSourceBatchId());
        }
    }
    @Test
    public void testQueryCheckError() {
        // 在测试方法中使用容器
        // 这里可以进行其他测试逻辑
        List<CuxNew> list = cuxNewService.queryErrorInfo("59ATS20230701657");
        for (CuxNew cuxNew: list) {
            System.out.println(cuxNew.getSourceBatchId()+cuxNew.getErrorMessage());
        }
    }
}
